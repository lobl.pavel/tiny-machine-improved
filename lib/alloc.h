/*
 *  This is a simple first fit allocator implemented using
 *  global array.
 *
 *  FIXME: Free block are added to the begining of the list
 *  and first fit block is choosen when allocationg a new
 *  chunk. This can lead to bad external fragmentation.
 *  Maybe we should search the whole list for best or good enough
 *  block or even keep free list sorted.
 *
 *  FIXME: To save some space, we should use ->next in block to
 *  store user data. Next pointer is not needed on used blocks.
 *
 *  Written by Pavel Löbl in 2015
 *
 */

#ifndef NULL
#define NULL ((void*)0)
#endif

#define __HEAP_SIZE 1024

#if DEBUG == 1
#define debug_print(...) \
            { if (DEBUG) fprintf(stderr, __VA_ARGS__); }
#else
#define debug_print(...) { ; }
#endif

#define malloc(s) tiny_malloc(s)
#define free(p) tiny_free(p)

struct block {
    unsigned int size;
    struct block *next;
};

struct {
    unsigned char data[__HEAP_SIZE];
    struct block *free_head;
} __heap;

void malloc_init() {
    __heap.free_head = (struct block*)__heap.data;
    __heap.free_head->next = NULL;
    __heap.free_head->size = __HEAP_SIZE - sizeof(struct block);
}

#if DEBUG == 1
void __heap_print() {
    printf("\n");
    printf("____HEAP_PRINT_BEGIN___\n");
    printf("base: 0x%p end: 0x%p\n", __heap.data, __heap.data + __HEAP_SIZE);
    struct block *blk = __heap.free_head;

    while (blk) {
        printf("free: %p next: %p size: %d\n", blk, blk->next, blk->size);
        blk = blk->next;
    }
    printf("_____HEAP_PRINT_END_____\n\n");
}
#endif

/* merge two adjecent blocks into one */
struct block* __heap_coalesce(struct block *prev, struct block *later) {
    prev->size += later->size + sizeof(struct block);
    return prev;
}

void __heap_split(struct block **prev_next, struct block *blk, int size) {
    int total_size = size + sizeof(struct block);
    debug_print("split: 0x%p size: %d blk_size: %d total_size: %d\n", blk, size, blk->size, total_size);

    if (blk->size > total_size) {
        struct block *new_free = (struct block*)(((unsigned char*)blk) + total_size);
        new_free->size = blk->size - total_size;
        debug_print("new_free_size: %d\n", new_free->size);
        new_free->next = blk->next;
        blk->size = size;
        *prev_next = new_free;
    } else {
        *prev_next = blk->next;
    }
    blk->next = NULL;
}

void* tiny_malloc(int size) {
    /* we don't support more then 254 bytes
     * chunks FIXME */
    struct block **prev_next = &__heap.free_head;
    struct block *blk = __heap.free_head;

    while (blk) {
        if (blk->size >= size) {
            __heap_split(prev_next, blk, size);
            debug_print("alloc: 0x%p %d\n", blk, size);
            return (((unsigned char*)blk) + sizeof(struct block));
        }

        debug_print("blk->next 0x%p\n", blk);
        prev_next = &blk->next;
        blk = blk->next;
    }
    return NULL;
}

/*
 *  case1:
 *  +--------+-/    /-+------------+----------+
 *  |   f0   |        |  to_free   |    fN    |
 *  +--------+-/    /-+------------+----------+
 *
 *  case2:
 *  +--------+------------+--/   /-+----------+
 *  |   f0   |   to_free  |        |    fN    |
 *  +--------+------------+--/   /-+----------+
 *
 *  case3:
 *  +--------+------------+--------+
 *  |   f0   |   to_free  |   fN   |
 *  +--------+------------+--------+
 *
 */

void tiny_free(void *ptr) {
    struct block *blk_to_free = (struct block*)(((unsigned char*)ptr) - sizeof(struct block));
    struct block *blk_free = __heap.free_head;
    struct block *to_free_next_in_array = (struct block*)((unsigned char*)blk_to_free
                                    + blk_to_free->size + sizeof(struct block));

    struct block **prev_next = &__heap.free_head;
    struct block **prev_blk = NULL;
    struct block **next_blk = NULL;

    debug_print("\nfree: %p\n", ptr);
    debug_print("free block: 0x%p %d %p\n", blk_to_free, blk_to_free->size, to_free_next_in_array);

    while (blk_free) {
        struct block *free_next_in_array = (struct block*)((unsigned char*)blk_free
                                    + blk_free->size + sizeof(struct block));
        debug_print("\tblk: 0x%p after: 0x%p before: 0x%p\n", blk_free, to_free_next_in_array, free_next_in_array);

        /* successive block */
        if (free_next_in_array == blk_to_free) {
            debug_print("\tbefore: 0x%p\n", blk_free);
            prev_blk = prev_next;
        }

        /* preceding block */
        if (blk_free == to_free_next_in_array) {
            debug_print("\tafter: 0x%p\n", blk_free);
            next_blk = prev_next;
        }

        prev_next = &blk_free->next;
        blk_free = blk_free->next;
    }

    /* handle case2 */
    if (prev_blk && !next_blk) {
        debug_print("case2: ");
        __heap_coalesce((*prev_blk), blk_to_free);
    }

    /* handle case1 */
    if (next_blk && !prev_blk) {
        debug_print("case1: ");
        __heap_coalesce(blk_to_free, *next_blk);
        blk_to_free->next = (*next_blk)->next;
        *next_blk = blk_to_free;
    }

    /* handle case3 */
    if (next_blk && prev_blk) {
        debug_print("case3: ");
        __heap_coalesce(*prev_blk, __heap_coalesce(blk_to_free, *next_blk));
        *next_blk = (*next_blk)->next;
    }

    debug_print("prev: %p next: %p\n", prev_blk, next_blk);

    /* if both preceding and sucessive blocks are allocated
     * no merging is possible, just add free block to the
     * beginning of the free-list */
    if (!prev_blk && !next_blk) {
        blk_to_free->next = __heap.free_head;
        __heap.free_head = blk_to_free;
    }
}
