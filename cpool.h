#ifndef CPOOL_H
#define CPOOL_H

#define CONSTANT_POOL_SIZE    1024
#define LINESIZE  121

struct cons_pool {
    struct { char name[LINESIZE]; int offset; } pool[CONSTANT_POOL_SIZE];
    int items;
};

int cons_pool_add(struct cons_pool *c, const char *constant, int offset);
int cons_pool_find_pos(struct cons_pool *c, const char *constant);
const char* cons_pool_offset_label(struct cons_pool *c, int offset);
int cons_pool_find(struct cons_pool *c, const char *constant);
int cons_pool_pos_offset(struct cons_pool *c, int pos);
void cons_pool_print(struct cons_pool *c);
void cons_pool_init(struct cons_pool *c);
int cons_pool_resolved(struct cons_pool *c);

#endif
