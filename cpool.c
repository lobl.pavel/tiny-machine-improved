#include "cpool.h"
#include "debug.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

void cons_pool_init(struct cons_pool *c) {
    c->items = 0;
}

void cons_pool_print(struct cons_pool *c) {
    int i;
    for (i = 0; i < c->items; i++) {
        printf("%02d: %10s -> %2d\n", i, c->pool[i].name, c->pool[i].offset);
    }
}

/* returns offset in memory (data or code) of symbol */
int cons_pool_find(struct cons_pool *c, const char *constant) {
    int i;
    for (i = 0; i < c->items; i++) {
        if (strcmp(c->pool[i].name, constant) == 0) {
            //printf("found offset: %d for %s\n", c->pool[i].offset, constant);
            return c->pool[i].offset;
        }
    }
    return -1;
}

int cons_pool_add(struct cons_pool *c, const char *constant, int offset) {
    DEBUG(printf("adding: '%s' offset %d\n", constant, offset);)
    int i;
    for (i = 0; i < c->items; i++) {
        if (strcmp(c->pool[i].name, constant) == 0) {
            if (c->pool[i].offset >= 0) {
                fprintf(stderr, "redefinition of label: %s\n", constant);
                return -1;
            }
            c->pool[i].offset = offset;
            return i;
        }
    }
    if (c->items == CONSTANT_POOL_SIZE) {
        fprintf(stderr, "constant pool is full, make it bigger and recompile");
        exit(EXIT_FAILURE);
    }
    c->pool[c->items].offset = offset;
    strncpy(c->pool[c->items].name, constant, LINESIZE);
    c->pool[c->items].name[LINESIZE - 1] = '\0';
    DEBUG(printf(" returns: %d\n", c->items);)
    return c->items++;
}

const char* cons_pool_offset_label(struct cons_pool *c, int offset) {
    int i;
    for (i = 0; i < c->items; i++) {
        if (c->pool[i].offset == offset)
            return c->pool[i].name;
    }
    return "?";
}

int cons_pool_resolved(struct cons_pool *c) {
    int resolved = 1;
    int i;
    for (i = 0; i < c->items; i++) {
        if (c->pool[i].offset == -1) {
            fprintf(stderr, "unresolved offset: '%s'\n",
                c->pool[i].name);
            resolved = 0;
        }
    }
    return resolved;
}
