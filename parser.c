/* This file contains simple parser for subset of gas (GNU assembler).
 * It's far from being complete gas parser. But it should be easy to
 * extend it later. */

#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <wctype.h>
#include <assert.h>
#include <string.h>

#include "parser.h"

#define DEBUG_LEXER(a) if (getenv("DEBUG_LEXER")) { a };
#define DEBUG_CALLBACKS(a) if (getenv("DEBUG_CALLBACKS")) { a };

/* don't change positions of items, they are used as indexs in transition
 * and action table in lexer */
enum input { IDIGIT, ILETTER, IMINUS, IPLUS, ISTAR, ISLASH, IEOF, IWS, ILF,
    IBSLASH, IQUOTE, ICOLON, ISCOLON, IOTHER, ICOMMA, ILPAR, IRPAR, IAT };

#define NUM_OF_INPUTS   18
#define NUM_OF_STATES   19

const char *tok_str[] = {
    "__start",
    "identifier",
    "-",
    "+",
    "*",
    "/",
    "number",
    "__accept",
    "error",
    "new line",
    "end of file",
    "__comment",
    "string",
    "__escstr",
    ":",
    ",",
    "(",
    ")",
    "@",

    "__dir_start",
    ".long",
    ".byte",
    ".short",
    ".text",
    ".data",
    ".type",
    ".file",
    ".balign",
    ".align",
    ".p2align",
    ".global",
    ".globl",
    ".size",
    ".section",
    ".string",
    ".ident",
    ".asciz",
    "__dir_end",

    "__opclass_start",
    "__lex_opclass_no_ops",
    "__lex_opclass_reg",
    "__lex_opclass_reg_reg",
    "__lex_opclass_reg_reg_reg",
    "__lex_opclass_reg_imm_reg",
    "__lex_opclass_reg_imm",
    "__lex_opclass_imm",
    "__opclass_end",

    "no token"
};

int state_trans[NUM_OF_STATES][NUM_OF_INPUTS] = {
/*                     0      1     2    3    4     5   6   7   8      9    10    11     12    13    14   15   16   17 */
/*                 DIGIT LETTER MINUS PLUS STAR SLASH EOF IWS  LF BSLASH QUOTE COLON SCOLON OTHER COMMA LPAR RPAR   AT  */
/*   START  0 */  {    6,     1,    2,   3,   4,    5, 10,  0,  9,     8,   12,   14,    11,    8,   15,  16,  17,  18 },
/*   IDENT  1 */  {    1,     1,    7,   7,   7,    7,  7,  7,  7,     8,    7,    7,     7,    8,    7,   7,   7,   7 },
/*     MIN  2 */  {    7,     7,    7,   7,   7,    7,  7,  7,  7,     8,    7,    7,     7,    8,    7,   7,   7,   7 },
/*    PLUS  3 */  {    7,     7,    7,   7,   7,    7,  7,  7,  7,     8,    7,    7,     7,    8,    7,   7,   7,   7 },
/*    STAR  4 */  {    7,     7,    7,   7,   7,    7,  7,  7,  7,     8,    7,    7,     7,    8,    7,   7,   7,   7 },
/*   SLASH  5 */  {    7,     7,    7,   7,   7,    7,  7,  7,  7,     8,    7,    7,     7,    8,    7,   7,   7,   7 },
/*   DIGIT  6 */  {    6,     7,    7,   7,   7,    7,  7,  7,  7,     8,    7,    7,     7,    8,    7,   7,   7,   7 },
/*  ACCEPT  7 */  {    0,     0,    0,   0,   0,    0,  0,  0,  0,     0,    0,    0,     0,    8,    0,   0,   0,   0 },
/*   ERROR  8 */  {    0,     0,    0,   0,   0,    0,  0,  0,  0,     0,    0,    0,     0,    8,    0,   0,   0,   0 },
/*      LF  9 */  {    7,     7,    7,   7,   7,    7,  7,  7,  7,     8,    7,    7,     7,    8,    7,   7,   7,   7 },
/*     EOF 17 */  {    7,     7,    7,   7,   7,    7,  7,  7,  7,     7,    7,    7,     7,    7,    7,   7,   7,   7 },
/* COMMENT 11 */  {   11,    11,   11,  11,  11,   11, 11, 11,  9,    11,   11,   11,    11,   11,   11,  11,  11,  11 },
/*     STR 12 */  {   12,    12,   12,  12,  12,   12, 12, 12, 12,    13,    7,   12,    12,   12,   12,  12,  12,  12 },
/*  ESCSTR 13 */  {    8,    12,    8,   8,   8,    8,  8,  8, 12,    12,    8,    8,     8,    8,    8,   8,   8,   7 },
/*   COLON 14 */  {    7,     7,    7,   7,   7,    7,  7,  7,  7,     8,    7,    7,     7,    8,   15,   7,   7,   7 },
/*   COMMA 15 */  {    7,     7,    7,   7,   7,    7,  7,  7,  7,     8,    7,    7,     7,    8,    7,   7,   7,   7 },
/*    LPAR 16 */  {    7,     7,    7,   7,   7,    7,  7,  7,  7,     8,    7,    7,     7,    8,    7,   7,   7,   7 },
/*    RPAR 17 */  {    7,     7,    7,   7,   7,    7,  7,  7,  7,     8,    7,    7,     7,    8,    7,   7,   7,   7 },
/*      AT 18 */  {    7,     7,    7,   7,   7,    7,  7,  7,  7,     8,    7,    7,     7,    8,    7,   7,   7,   7 }
};

void lexer_init(struct lexer *l, FILE *in) {
    l->input = in;
    l->state = 0;
    l->buffp = l->buffer;
    l->line = 1;
}

enum token op_to_tok(enum opcode op) {
    enum token tok;
    switch (op) {
        case OP_CLASS_NO_OPS:
            tok = LEX_OPCLASS_NO_OPS;
            break;
        case OP_CLASS_REG:
            tok = LEX_OPCLASS_REG;
            break;
        case OP_CLASS_REG_REG:
            tok = LEX_OPCLASS_REG_REG;
            break;
        case OP_CLASS_REG_REG_REG:
            tok = LEX_OPCLASS_REG_REG_REG;
            break;
        case OP_CLASS_REG_IMM_REG:
            tok = LEX_OPCLASS_REG_IMM_REG;
            break;
        case OP_CLASS_REG_IMM:
            tok = LEX_OPCLASS_REG_IMM;
            break;
        case OP_CLASS_INVALID:
            tok = LEX_NO_TOK;
            break;
        case OP_CLASS_IMM:
            tok = LEX_OPCLASS_IMM;
            break;
        default:
            assert(0 && "invalid opcode received");
    }
    return tok;
}

int epsi(struct lexer *l) {
    ungetc(l->c, l->input);
    return 0;
}

int append(struct lexer *l) {
    *l->buffp++ = l->c;
    *l->buffp = '\0';
    return 0;
}

int clear_append(struct lexer *l) {
    l->buffp = l->buffer;
    append(l);
    return 0;
}

int lexer_get_num(struct lexer *l) {
    return atoi(l->buffer);
}

char *lexer_get_str(struct lexer *l) {
    return l->buffer;
}

int (*state_trans_actions[NUM_OF_STATES][NUM_OF_INPUTS])(struct lexer *l) = {
/*                     0              1             2     3      4      5     6      7     8          9     10     11      12      13    14    15    16   17 */
/*                 DIGIT         LETTER         MINUS  PLUS   STAR   SLASH  EOF    IWS    LF     BSLASH  QUOTE  COLON  SCOLON   OTHER COMMA  LPAR  RPAR   AT */
/*   START  0 */ { clear_append, clear_append,  NULL,  NULL,  NULL,  NULL,  NULL,  NULL,  NULL,  NULL,   NULL,  NULL,    NULL,  NULL,  NULL, NULL, NULL, NULL },
/*   IDENT  1 */ {       append,       append,  epsi,  epsi,  epsi,  epsi,  epsi,  epsi,  epsi,  NULL,   epsi,  epsi,    epsi,  NULL,  epsi, NULL, epsi, epsi },
/*     MIN  2 */ {         epsi,         epsi,  epsi,  epsi,  epsi,  epsi,  epsi,  epsi,  epsi,  NULL,   epsi,  epsi,    epsi,  NULL,  NULL, epsi, epsi, epsi },
/*    PLUS  3 */ {         epsi,         epsi,  epsi,  epsi,  epsi,  epsi,  epsi,  epsi,  epsi,  NULL,   epsi,  epsi,    epsi,  NULL,  NULL, epsi, NULL, epsi },
/*    STAR  4 */ {         epsi,         epsi,  epsi,  epsi,  epsi,  epsi,  epsi,  epsi,  epsi,  NULL,   epsi,  epsi,    epsi,  NULL,  NULL, epsi, NULL, epsi },
/*   SLASH  5 */ {         epsi,         epsi,  epsi,  epsi,  epsi,  epsi,  epsi,  epsi,  epsi,  NULL,   epsi,  epsi,    epsi,  NULL,  NULL, epsi, NULL, epsi },
/*   DIGIT  6 */ {       append,         NULL,  epsi,  epsi,  epsi,  epsi,  epsi,  epsi,  epsi,  NULL,   epsi,  epsi,    epsi,  NULL,  epsi, epsi, epsi, epsi },
/*      OK  7 */ {         NULL,         NULL,  NULL,  NULL,  NULL,  NULL,  NULL,  NULL,  NULL,  NULL,   NULL,  NULL,    NULL,  NULL,  NULL, NULL, NULL, NULL },
/*   ERROR  8 */ {         NULL,         NULL,  NULL,  NULL,  NULL,  NULL,  NULL,  NULL,  NULL,  NULL,   NULL,  NULL,    NULL,  NULL,  NULL, NULL, NULL, NULL },
/*      LF  9 */ {         epsi,         epsi,  epsi,  epsi,  epsi,  epsi,  epsi,  epsi,  epsi,  NULL,   epsi,  epsi,    epsi,  NULL,  NULL, NULL, NULL, epsi },
/*     EOF 10 */ {         NULL,         NULL,  NULL,  NULL,  NULL,  NULL,  NULL,  NULL,  NULL,  NULL,   NULL,  NULL,    NULL,  NULL,  NULL, NULL, NULL, NULL },
/* COMMENT 11 */ {         NULL,         NULL,  NULL,  NULL,  NULL,  NULL,  NULL,  NULL,  NULL,  NULL,   NULL,  NULL,    NULL,  NULL,  NULL, NULL, NULL, NULL },
/*     STR 12 */ {       append,       append,append,append,append,append,append,append,append,append,   NULL,append,  append,append,append,append,append,append},
/*  ESCSTR 13 */ {         NULL,       append,  NULL,  NULL,  NULL,  NULL,  NULL,  NULL,append,append,   NULL,  NULL,    NULL,  NULL,  NULL, NULL, NULL, NULL },
/*   COLON 14 */ {         epsi,         epsi,  epsi,  epsi,  epsi,  epsi,  epsi,  epsi,  epsi,  NULL,   epsi,  epsi,    epsi,  NULL,  NULL, NULL, NULL, epsi },
/*   COMMA 15 */ {         epsi,         epsi,  epsi,  epsi,  epsi,  epsi,  epsi,  epsi,  epsi,  NULL,   epsi,  epsi,    epsi,  NULL,  NULL, NULL, NULL, epsi },
/*    LPAR 16 */ {         epsi,         epsi,  epsi,  epsi,  epsi,  epsi,  epsi,  epsi,  epsi,  NULL,   epsi,  epsi,    epsi,  NULL,  NULL, NULL, NULL, epsi },
/*    RPAR 17 */ {         epsi,         epsi,  epsi,  epsi,  epsi,  epsi,  epsi,  epsi,  epsi,  NULL,   epsi,  epsi,    epsi,  NULL,  NULL, NULL, NULL, epsi },
/*      AT 18 */ {         epsi,         epsi,  epsi,  epsi,  epsi,  epsi,  epsi,  epsi,  epsi,  NULL,   epsi,  epsi,    epsi,  NULL,  NULL, NULL, NULL, epsi }
};

enum token dir_from_str(const char *str) {
    int i;
    for (i = __LEX_DIR_START + 1; i < __LEX_DIR_END;  ++i) {
        if (strcmp(str, tok_str[i]) == 0)
            return i;
    }
    return LEX_NO_TOK;
}

char *lexer_get_escaped_str(struct lexer *l) {
    char *str = l->buffer;
    char *dstr = str;
    char c;
    int escape = 0;
    while ((c = *str) != '\0') {
        if (escape) {
            switch (c) {
                case '\\': c = '\\'; break;
                case 'n': c = '\n'; break;
                case 'r': c = '\r'; break;
                case 't': c = '\t'; break;
                case 'v': c = '\v'; break;
                case 'b': c = '\b'; break;
                case 'f': c = '\f'; break;
                case '0': c = '\0'; break;
            }
            escape = 0;
        } else if (c == '\\') {
            escape = 1;
            str++;
            continue;
        }
        *dstr = c;
        dstr++;
        str++;
    }
    *dstr = '\0';
    return l->buffer;
}

enum token lexer_get_token(struct lexer *l) {
    /* clear buffer */
    l->buffp = l->buffer;
    l->state = __START;
    for (;;) {
        l->c = fgetc(l->input);
        enum input input_tok;
            DEBUG_LEXER(printf("input: '%c'\n", l->c););
        if (isdigit(l->c))
            input_tok = IDIGIT;
        else if (l->c == '\n')
            input_tok = ILF;
        else if (iswspace(l->c) && l->c != '\n')
            input_tok = IWS;
        else if ((isalpha(l->c) || l->c == '.' || l->c == '_') && !iswspace(l->c))
            input_tok = ILETTER;
        else if (l->c == '\\')
            input_tok = IBSLASH;
        else if (l->c == '"')
            input_tok = IQUOTE;
        else if (l->c == '/')
            input_tok = ISLASH;
        else if (l->c == '*')
            input_tok = ISTAR;
        else if (l->c == '+')
            input_tok = IPLUS;
        else if (l->c == '-')
            input_tok = IMINUS;
        else if (l->c == EOF)
            input_tok = IEOF;
        else if (l->c == ':')
            input_tok = ICOLON;
        else if (l->c == ';')
            input_tok = ISCOLON;
        else if (l->c == ',')
            input_tok = ICOMMA;
        else if (l->c == '(')
            input_tok = ILPAR;
        else if (l->c == ')')
            input_tok = IRPAR;
        else if (l->c == '@')
            input_tok = IAT;
        else {
            /* we should allow more chars in comments */
            input_tok = IOTHER;
        }

        int next_state = state_trans[l->state][input_tok];

        if (state_trans_actions[l->state][input_tok])
            state_trans_actions[l->state][input_tok](l);

        if (next_state == __ACCEPT) {
            enum token tok = l->state;
            char *str = lexer_get_str(l);
            if (tok == LEX_IDENT) {
                enum token keyword;
                if ((keyword = dir_from_str(str)) != LEX_NO_TOK)
                    tok = keyword;
                keyword = op_to_tok(opcode_class(opcode_from_str(str)));
                if (keyword != LEX_NO_TOK)
                    tok = keyword;
            }
            if (tok == LEX_LF)
                l->line++;

            DEBUG_LEXER(
                if (tok == LEX_IDENT || (tok > __OPCLASS_START && tok < __OPCLASS_END) ||
                    tok == LEX_NUM)
                    printf("TOKEN: %s (%s)\n", tok_str[tok], str);
                else
                    printf("TOKEN: %s\n", tok_str[tok]);
            );

            return tok;
        }

        if (next_state == LEX_ERROR) {
            DEBUG_LEXER(printf("TOKEN: %s\n", tok_str[LEX_ERROR]););
            return LEX_ERROR;
        }

        if (getenv("DEBUG_INPUT"))
            printf("transition: %d -> %d (input %d)\n", l->state, next_state, input_tok);
        l->state = next_state;
    }
}

int list_cmp(const void *a, const void *b, void *data) {
    return strcmp(a, b);
}

struct ast {
    struct ast_node *root;
    struct ast_callbacks *calls;
};

void ast_init(struct ast *tree, struct ast_callbacks *hooks) {
    tree->calls = hooks;
    tree->root = NULL;
}

struct ast_node *ast_node_create() {
    struct ast_node *n = malloc(sizeof(struct ast_node));
    if (n == NULL) {
        fprintf(stderr, "cannot allocate memory for AST node\n");
        exit(EXIT_FAILURE);
    }
    return n;
}

int eval_inst(struct ast_node *n) {
    int arg1 = 0, arg2 = 0, arg3 = 0;

    if (n->child[0])
        arg1 = n->child[0]->eval(n->child[0]);
    if (n->child[1])
        arg2 = n->child[1]->eval(n->child[1]);
    if (n->child[2])
        arg3 = n->child[2]->eval(n->child[2]);

    DEBUG_CALLBACKS(printf("eval_inst: %s %d %d %d offset: %d\n",
        opcode_to_str(n->val.inst.op), arg1, arg2, arg3, n->val.inst.offset););

    n->cb->instruction(n->val.inst.op, arg1, arg2, arg3,
        n->val.inst.offset, n->cb->user_data);

    if (n->next)
        n->next->eval(n->next);

    return 0;
}

int eval_long(struct ast_node *n) {
    uint32_t val = n->child[0]->eval(n->child[0]);
    n->cb->store_long(val, n->val.offset, n->cb->user_data);

    DEBUG_CALLBACKS(printf("eval_long: %d offset: %d\n", n->val.offset, val););

    if (n->next)
        n->next->eval(n->next);

    return 0;
}

int eval_label(struct ast_node *n) {
    return n->cb->label_address(n->val.label, n->cb->user_data);
}

struct ast_node *ast_node_create_inst(enum opcode op,
        struct ast_node *arg1, struct ast_node *arg2,
        struct ast_node *arg3, int offset, struct ast_callbacks *cb) {
    struct ast_node *inst = ast_node_create();
    inst->val.inst.op = op;
    inst->val.inst.offset = offset;
    inst->child[0] = arg1;
    inst->child[1] = arg2;
    inst->child[2] = arg3;
    inst->eval = &eval_inst;
    inst->cb = cb;
    return inst;
}

struct ast_node *ast_node_create_label(char *label, struct ast_callbacks *cb) {
    struct ast_node *l = ast_node_create();
    l->cb = cb;
    l->eval = eval_label;
    l->val.label = strdup(label);
    return l;
}

int eval_unminus(struct ast_node *n) {
    return n->child[0]->eval(n->child[0]) * -1;
}

int eval_mult(struct ast_node *n) {
    return n->child[0]->eval(n->child[0]) * n->child[1]->eval(n->child[1]);
}

int eval_div(struct ast_node *n) {
    return n->child[0]->eval(n->child[0]) / n->child[1]->eval(n->child[1]);
}

int eval_plus(struct ast_node *n) {
    return n->child[0]->eval(n->child[0]) + n->child[1]->eval(n->child[1]);
}

int eval_minus(struct ast_node *n) {
    return n->child[0]->eval(n->child[0]) - n->child[1]->eval(n->child[1]);
}

int eval_num(struct ast_node *n) {
    return n->val.num;
}

struct ast_node *ast_node_create_int(int num) {
    struct ast_node *n = ast_node_create();
    n->val.num = num;
    n->eval = &eval_num;
    return n;
}

struct ast_node *ast_node_create_long(struct ast_node *num, unsigned offset,
        struct ast_callbacks *cb) {
    struct ast_node *n = ast_node_create();
    n->val.offset = offset;
    n->child[0] = num;
    n->cb = cb;
    n->eval = &eval_long;
    return n;
}

void parser_dir_align(struct parser *p, int align) {
    p->mem_index = ((p->mem_index + align - 1) / align) * align;
}

void parser_inc_pc(struct parser *p, int advance) {
    p->mem_index += advance;
}

/* ================= PARSER ======================= */

void parser_unexpected(struct parser *p, enum token tok, enum token expected[]) {
    fprintf(stderr, "unexpected %s expected [", tok_str[tok]);
    int i;
    for (i = 0; expected[i] != LEX_NO_TOK; i++) {
        fprintf(stderr, "%s", tok_str[expected[i]]);
        if (expected[i + 1] != LEX_NO_TOK)
            fprintf(stderr, ", ");
    }
    fprintf(stderr, "] on line %d\n", p->lex.line);
    exit(EXIT_FAILURE);
}

void parser_init(struct parser *p, FILE *in, struct ast_callbacks cb) {
    lexer_init(&p->lex, in);
    p->cb = cb;
    p->tok = LEX_NO_TOK;
    p->eof = 0;
    p->mem_index = 16;
}


enum token parser_look(struct parser *p) {
    if (p->tok == LEX_NO_TOK)
        p->tok = lexer_get_token(&p->lex);
    return p->tok;
}

void parser_shift(struct parser *p) {
    p->tok = LEX_NO_TOK;
}

int parser_match(struct parser *p, enum token tok) {
    enum token got = parser_look(p);

    if (got == LEX_ERROR)
        return 0;

    if (got == tok) {
        parser_shift(p);
        return 1;
    } else {
        parser_unexpected(p, got, (enum token[]){ tok, LEX_NO_TOK });
        return 0;
    }
}

void parser_store_string(struct parser *p, char *str) {
    while (*str) {
        DEBUG_CALLBACKS(printf("store_byte: %u(%c), %d\n",
            *str, *str, p->mem_index););
        p->cb.store_byte(*str++, p->mem_index++, p->cb.user_data);
    }
    parser_inc_pc(p, strlen(str) + 1);
}

void parser_label_add(struct parser *p, char *symbol) {
    if (p->cur_section == SECTION_TEXT) {
        p->cb.label_add(symbol, p->mem_index, p->cb.user_data);
    } else {
        p->cb.label_add(symbol, p->mem_index, p->cb.user_data);
    }
}

struct ast_node *parser_expr(struct parser *p);

struct ast_node *parser_factor(struct parser *p) {
    enum token tok;

    if ((tok = parser_look(p)) == LEX_ERROR)
        return NULL;

    struct ast_node *new_node = ast_node_create();
    switch (tok) {
        /* FIXME handle parentheses */
        default:
            parser_unexpected(p, tok, (enum token[]){
                LEX_NUM, LEX_MINUS, LEX_NO_TOK});
        case LEX_NUM:
            parser_match(p, LEX_NUM);
            new_node->val.num = lexer_get_num(&p->lex);
            new_node->eval = eval_num;
            break;
        case LEX_IDENT:
            parser_match(p, LEX_IDENT);
            new_node->eval = eval_label;
            new_node->val.label = strdup(lexer_get_str(&p->lex));
            new_node->cb = &p->cb;
            break;
        case LEX_MINUS:
            parser_match(p, LEX_MINUS);
            new_node->eval = eval_unminus;
            new_node->child[0] = parser_factor(p);
            break;
        case LEX_LPAR:
            parser_match(p, LEX_LPAR);
            free(new_node);
            new_node = parser_expr(p);
            parser_match(p, LEX_RPAR);
            break;
    }
    return new_node;
}

struct ast_node *parser_term2(struct parser *p, struct ast_node *lfact) {
    enum token tok;

    if ((tok = parser_look(p)) == LEX_ERROR)
        return NULL;

    struct ast_node *new_node = ast_node_create();
    switch (tok) {
        default:
            parser_unexpected(p, tok, (enum token[]){
                LEX_SLASH, LEX_STAR, LEX_MINUS, LEX_PLUS, LEX_LF, LEX_EOF, LEX_NO_TOK});
            return NULL;
        case LEX_SLASH:
            if (!parser_match(p, LEX_SLASH))
                return NULL;
            new_node->child[0] = lfact;
            new_node->child[1] = parser_factor(p);
            new_node->eval = eval_div;
            return parser_term2(p, new_node);
        case LEX_STAR:
            if (!parser_match(p, LEX_STAR))
                return NULL;
            new_node->child[0] = lfact;
            new_node->child[1] = parser_factor(p);
            new_node->eval = eval_mult;
            return parser_term2(p, new_node);
        case LEX_EOF:
        case LEX_MINUS:
        case LEX_PLUS:
        case LEX_LF:
        case LEX_RPAR:
            return lfact;
    }
    return new_node;
}

struct ast_node *parser_term(struct parser *p) {
    return parser_term2(p, parser_factor(p));
}

struct ast_node *parser_expr2(struct parser *p, struct ast_node *lterm) {

    enum token tok;

    if ((tok = parser_look(p)) == LEX_ERROR)
        return NULL;

    struct ast_node *new_node = ast_node_create();
    switch (tok) {
        default:
            parser_unexpected(p, tok, (enum token[]){ LEX_PLUS, LEX_MINUS, LEX_NO_TOK });
            assert(0);
        case LEX_PLUS:
            parser_match(p, LEX_PLUS);
            new_node->child[0] = lterm;
            new_node->child[1] = parser_term(p);
            new_node->eval = eval_plus;
            return parser_expr2(p, new_node);
        case LEX_MINUS:
            parser_match(p, LEX_MINUS);
            new_node->child[0] = lterm;
            new_node->child[1] = parser_term(p);
            new_node->eval = eval_minus;
            return parser_expr2(p, new_node);
        case LEX_EOF:
        case LEX_LF:
        case LEX_RPAR:
            return lterm;
    }
    return new_node;
}

struct ast_node *parser_expr(struct parser *p) {
    return parser_expr2(p, parser_term(p));
}

struct ast_node *parser_line(struct parser *p) {

    enum token tok = parser_look(p);
    struct ast_node *line = NULL;

    struct ast_node *arg1 = NULL, *arg2 = NULL, *arg3 = NULL;

//    int arg1, arg2, arg3;
    int minus = 1;
    char *symbol;
    enum opcode op;

    switch (tok) {
        case LEX_DIR_TEXT:
            parser_match(p, LEX_DIR_TEXT);
            p->cur_section = SECTION_TEXT;
            break;
        case LEX_DIR_DATA:
            parser_match(p, LEX_DIR_DATA);
            // TODO: switch to data section
            break;
        case LEX_DIR_TYPE:
            parser_match(p, LEX_DIR_TYPE);
            parser_match(p, LEX_IDENT);
            parser_match(p, LEX_COMMA);
            parser_match(p, LEX_AT);
            parser_match(p, LEX_IDENT);
            break;
        case LEX_DIR_BALIGN:
        case LEX_DIR_P2ALIGN:
        case LEX_DIR_ALIGN:
            parser_match(p, tok);
            arg1 = parser_expr(p);
            parser_dir_align(p, arg1->eval(arg1));
            break;
        case LEX_DIR_LONG:
            parser_match(p, LEX_DIR_LONG);
            line = ast_node_create_long(parser_expr(p), p->mem_index, &p->cb);
            parser_inc_pc(p, 4);
            break;
        case LEX_DIR_BYTE:
        case LEX_DIR_SHORT:
            break;
        case LEX_DIR_FILE:
            if (!parser_match(p, LEX_DIR_FILE) ||
                !parser_match(p, LEX_STRING))
                return NULL;
            break;
        case LEX_DIR_GLOBL:
        case LEX_DIR_GLOBAL:
            if (!parser_match(p, tok) ||
                !parser_match(p, LEX_IDENT))
                return NULL;
            break;
        case LEX_DIR_SIZE:
            if (!parser_match(p, LEX_DIR_SIZE) ||
                !parser_match(p, LEX_IDENT) ||
                !parser_match(p, LEX_COMMA))
                return NULL;
            /* XXX need to be saved? */
            if (!parser_expr2(p, parser_term(p)))
                return NULL;
            break;
        case LEX_DIR_SECTION:
            parser_match(p, LEX_DIR_SECTION);
            parser_match(p, LEX_IDENT);
            if (strcmp(lexer_get_str(&p->lex), ".rodata") == 0) {
                p->cur_section = SECTION_DATA;
            }
            if (parser_look(p) == LEX_COMMA) {
                parser_match(p, LEX_COMMA);
                parser_match(p, LEX_STRING);
            }
            if (parser_look(p) == LEX_COMMA) {
                parser_match(p, LEX_COMMA);
                parser_match(p, LEX_AT);
                parser_match(p, LEX_IDENT);
            }
            if (parser_look(p) == LEX_COMMA) {
                parser_match(p, LEX_COMMA);
                parser_match(p, LEX_NUM);
            }
            break;
        case LEX_DIR_ASCIZ:
        case LEX_DIR_STRING:
            parser_match(p, tok);
            parser_match(p, LEX_STRING);
            parser_store_string(p, lexer_get_escaped_str(&p->lex));
            parser_store_string(p, "\0");
            break;
        case LEX_DIR_IDENT:
            if (!parser_match(p, LEX_DIR_IDENT) ||
                !parser_match(p, LEX_STRING))
                return NULL;
            break;
        case LEX_IDENT:
            parser_match(p, LEX_IDENT);
            symbol = lexer_get_str(&p->lex);
            parser_match(p, LEX_COLON);
            parser_label_add(p, symbol);
            break;
        case LEX_OPCLASS_REG_IMM_REG:
            parser_match(p, LEX_OPCLASS_REG_IMM_REG);
            op = opcode_from_str(lexer_get_str(&p->lex));
            parser_match(p, LEX_NUM);
            arg1 = ast_node_create_int(lexer_get_num(&p->lex));
            parser_match(p, LEX_COMMA);
            if (parser_look(p) == LEX_MINUS) {
                parser_match(p, LEX_MINUS);
                minus = -1;
            }
            parser_match(p, LEX_NUM);
            arg2 = ast_node_create_int(minus * lexer_get_num(&p->lex));
            parser_match(p, LEX_LPAR);
            parser_match(p, LEX_NUM);
            arg3 = ast_node_create_int(lexer_get_num(&p->lex));
            parser_match(p, LEX_RPAR);
            break;
        case LEX_OPCLASS_REG_REG:
            parser_match(p, LEX_OPCLASS_REG_REG);
            op = opcode_from_str(lexer_get_str(&p->lex));
            parser_match(p, LEX_NUM);
            arg1 = ast_node_create_int(lexer_get_num(&p->lex));
            parser_match(p, LEX_COMMA);
            parser_match(p, LEX_NUM);
            arg2 = ast_node_create_int(lexer_get_num(&p->lex));
            break;
        case LEX_OPCLASS_REG:
            parser_match(p, LEX_OPCLASS_REG);
            op = opcode_from_str(lexer_get_str(&p->lex));
            parser_match(p, LEX_NUM);
            arg1 = ast_node_create_int(lexer_get_num(&p->lex));
            break;
        case LEX_OPCLASS_REG_IMM:
            parser_match(p, LEX_OPCLASS_REG_IMM);
            op = opcode_from_str(lexer_get_str(&p->lex));
            parser_match(p, LEX_NUM);
            arg1 = ast_node_create_int(lexer_get_num(&p->lex));
            parser_match(p, LEX_COMMA);
            switch (parser_look(p)) {
                case LEX_IDENT:
                    arg2 = parser_expr(p);
                    break;
                case LEX_MINUS:
                    parser_match(p, LEX_MINUS);
                    minus = -1;
                case LEX_NUM:
                    parser_match(p, LEX_NUM);
                    arg2 = ast_node_create_int(minus * lexer_get_num(&p->lex));
                    break;
                default:
                    return NULL;
            }
            break;
        case LEX_OPCLASS_REG_REG_REG:
            parser_match(p, LEX_OPCLASS_REG_REG_REG);
            op = opcode_from_str(lexer_get_str(&p->lex));
            parser_match(p, LEX_NUM);
            arg1 = ast_node_create_int(lexer_get_num(&p->lex));
            parser_match(p, LEX_COMMA);
            parser_match(p, LEX_NUM);
            arg2 = ast_node_create_int(lexer_get_num(&p->lex));
            parser_match(p, LEX_COMMA);
            parser_match(p, LEX_NUM);
            arg3 = ast_node_create_int(lexer_get_num(&p->lex));
            break;
        case LEX_OPCLASS_NO_OPS:
            parser_match(p, LEX_OPCLASS_NO_OPS);
            op = opcode_from_str(lexer_get_str(&p->lex));
            break;
        case LEX_OPCLASS_IMM:
            parser_match(p, LEX_OPCLASS_IMM);
            op = opcode_from_str(lexer_get_str(&p->lex));
            arg1 = parser_expr(p);
            break;
        case LEX_LF:
            break;
        case LEX_EOF:
            return NULL;
        default:
            parser_unexpected(p, tok, (enum token[]){ LEX_DIR_TEXT, LEX_DIR_BALIGN, LEX_DIR_LONG,
    LEX_DIR_BYTE, LEX_DIR_SHORT, LEX_DIR_FILE, LEX_DIR_GLOBAL, LEX_DIR_GLOBL, LEX_DIR_SIZE, LEX_DIR_SECTION, LEX_DIR_STRING,
    LEX_DIR_IDENT, LEX_IDENT, LEX_OPCLASS_REG_IMM_REG, LEX_OPCLASS_REG_REG, LEX_OPCLASS_REG, LEX_OPCLASS_REG_IMM,
    LEX_OPCLASS_REG_REG_REG, LEX_OPCLASS_NO_OPS, LEX_OPCLASS_IMM, LEX_LF, LEX_EOF, LEX_NO_TOK });
            return NULL;
    }

    switch(tok) {
        case LEX_OPCLASS_NO_OPS:
        case LEX_OPCLASS_REG:
        case LEX_OPCLASS_REG_REG:
        case LEX_OPCLASS_REG_REG_REG:
        case LEX_OPCLASS_REG_IMM_REG:
        case LEX_OPCLASS_REG_IMM:
        case LEX_OPCLASS_IMM:
            line = ast_node_create_inst(op, arg1, arg2, arg3, p->mem_index, &p->cb);
            parser_inc_pc(p, opcode_length(op));
            break;
        default:
            break;
    }

    if (!parser_match(p, LEX_LF))
        return NULL;

    struct ast_node *next_line = parser_line(p);

    if (line) {
        line->next = next_line;
    } else {
        line = next_line;
    }

    return line;
}
